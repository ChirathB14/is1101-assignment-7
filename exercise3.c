/** This program add and multiply two given matrixes.
 */
#include<stdio.h>

#define MAX_ROWS 100 //maximum number of rows
#define MAX_COLS 100 //maximum number of columns

int main()
{
    /* declaring of arrays and vraibles*/
	int arr1[MAX_ROWS][MAX_COLS],
		arr2[MAX_ROWS][MAX_COLS],
		sum[MAX_ROWS][MAX_COLS] = {0},
		product[MAX_ROWS][MAX_COLS] = {0},
		matrixlen;

    /*  To do both multiplication both matrices should be 
     *  square matrices.
     */
	printf("\nYou can add and multiply any 2 square matrcies using this program!\n\n");

    /* prompting user input*/
    printf("Enter the number element  square matrix  : ");
	scanf("%d", &matrixlen);
    
    /*Prompting values for array 1*/
	printf("\nEnter numbers for array 1: \n");
	for (int i = 0; i < matrixlen; i++)
	{
		for (int j = 0; j < matrixlen; j++)
		{
			printf("Enter number for row %d column %d : ", i+1, j+1);
			scanf("%d", &arr1[i][j]);
		}
	}

    /*Prompting values for array 2*/
    printf("\nEnter numbers for array 2: \n");
	for (int i = 0; i < matrixlen; i++)
	{
		for (int j = 0; j < matrixlen; j++)
		{
			printf("Enter number for row %d column %d : ", i+1, j+1);
			scanf("%d", &arr2[i][j]);
		}
	}
    

    /* Addition of 2 matrices*/
	for (int i = 0; i < matrixlen; i++)
	{
		for (int j = 0; j < matrixlen; j++)
		{
			sum[i][j] = arr1[i][j] + arr2[i][j];
		}
			
	}

    /* prints the result matrix of summation*/
	printf("\nSum of 2 matrices : \n\n");
	for (int i = 0; i < matrixlen; ++i)
	{
		for (int j = 0; j < matrixlen; ++j)
		{
			printf("%3d   ", sum[i][j]);

			if (j == matrixlen - 1)
			{
				printf("\n\n");
			}
		}
	}
	printf("\n");

    /* Multiplication 2 matrices*/
	for (int i = 0; i < matrixlen; i++)
	{
		for (int j = 0; j < matrixlen; j++)
		{
			for (int k = 0; k < matrixlen; k++)
            {
				product[i][j] = product[i][j] + arr1[i][k] * arr2[k][j];
            }
		}
	}

    /*Prints the result matrix of multiplication*/
	printf("\nMultiplication of 2 matrices\n\n");
	for (int i = 0; i < matrixlen; ++i)
	{
		for (int j = 0; j < matrixlen; ++j)
		{
			printf("%3d   ", product[i][j]);

			if (j == matrixlen - 1)
			{
				printf("\n\n");
			}
		}
	}
	printf("\n");
		
	return 0;
}