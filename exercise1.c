/**
 * C program to reverse a sentence entered by user using 
 * strrev library function.
 */
#include <stdio.h>
#include <string.h> //using string library

#define SIZE 100   //constant to define the size of array

//main function
int main()
{
    char inputarr[SIZE];

    /* prompting input from user */
    printf("Enter a sentence to reverse \t: ");
    gets(inputarr);

    /* in-built string reverse function */
    strrev(inputarr); 

    /* printing reversed sentence */
    printf("Reversed sentence \t\t: %s", inputarr);

    return 0;
}