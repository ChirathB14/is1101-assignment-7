/**
 * C program to find the Frequency of a given character in a given string.
 */

#include <stdio.h>
#include <string.h> //using string library

#define SIZE 100 //constant to define the size of array

//main function
int main()
{
    char inputarr[SIZE], checkletter;
    int frequency = 0;

    /* prompting sentence from user */
    printf("Enter a sentence \t\t: ");
    gets(inputarr);
    /* prompting letter to search */
    printf("Enter a letter to search\t: ");
    scanf("%c", &checkletter);

    /* this loop iterates through the array until the end*/
    for (int i = 0; inputarr[i] != '\0'; i++)
    {
        /* condtion to check the current letter and increment
         * the frequency variable if the current letter matches 
         * to input letter
         */
        if (inputarr[i] == checkletter )
        {
            frequency++;
        }
        
    }

    /* printing the frequency */
    printf("Frequency of %c is %d\n", checkletter, frequency);

    return 0;
}